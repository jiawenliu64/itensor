#include "itensor/all.h"
using namespace itensor;

int 
main()
    {
    //int N = 100;
    int N = 10000; 

    //
    // Initialize the site degrees of freedom
    // Setting "ConserveQNs=",true makes the indices
    // carry Sz quantum numbers and will lead to 
    // block-sparse MPO and MPS tensors
    //
    //auto sites = SpinHalf(N); //make a chain of N spin 1/2's
    auto sites = SpinOne(N, {"ConserveQNs=", true}); //make a chain of N spin 1's

    //
    // Use the AutoMPO feature to create the 
    // next-neighbor Heisenberg model
    //
    auto ampo = AutoMPO(sites);
    

    for(auto j : range1(N-1))
        {
        ampo += 0.5,"S+",j,"S-",j+1;
        ampo += 0.5,"S-",j,"S+",j+1;
        ampo +=     "Sz",j,"Sz",j+1;
        }
    auto H = toMPO(ampo);

    // Set the initial wavefunction matrix product state
    // to be a Neel state.
    //
    auto state = InitState(sites);

    for(auto i : range1(N))
        {
        if(i%2 == 1) state.set(i,"Up");
        else         state.set(i,"Dn");
        }
    auto psi0 = MPS(state);

    //
    // inner calculates matrix elements of MPO's with respect to MPS's
    // inner(psi,H,psi) = <psi|H|psi>
    //
    printfln("Initial energy = %.5f", inner(psi0,H,psi0) );

    //
    // Set the parameters controlling the accuracy of the DMRG
    // calculation for each DMRG sweep. 
    // Here less than 5 cutoff values are provided, for example,
    // so all remaining sweeps will use the last one given (= 1E-10).
    //
    auto sweeps = Sweeps(1);
    /*sweeps.maxdim() = 10,20,100,100,200;
    sweeps.cutoff() = 1E-10;
    sweeps.niter() = 2;
    sweeps.noise() = 1E-7,1E-8,0.0;*/
    sweeps.maxdim() = 10,20,100,100,200;
    sweeps.cutoff() = 1E-40;
    sweeps.niter() = 2;
    sweeps.noise() = 1E-7,1E-8,0.0;
    /*sweeps.maxdim() = 500,500,500,500;
    sweeps.cutoff() = 1E-10;
    sweeps.niter() = 1;
    sweeps.noise() = 1E-280,1E-320,1E-200;*/
    println(sweeps);

    //
    // Begin the DMRG calculation
    //
    auto [energy,psi] = dmrg(H,psi0,sweeps,"Quiet");

    //To calculate sparsity
    auto nnz_tot = 0;
    auto dim_tot = 0;
    for(auto i : range1(N))
        {
        nnz_tot += nnz(H(i));
        dim_tot += dim(inds(H(i)));
        }
    //printf("Input 1: \n");    
    //PrintData(nnz_tot);
    //PrintData(dim_tot);
    //printfln("\n");

    //PrintData(H);

    //To calculate sparsity
    nnz_tot = 0;
    dim_tot = 0;
    for(auto i : range1(N))
        {
        nnz_tot += nnz(psi0(i));
        dim_tot += dim(inds(psi0(i)));
        }
    /*printf("Input 2: \n");    
    PrintData(nnz_tot);
    PrintData(dim_tot);
    printfln("\n");

    PrintData(psi0);*/

    //To calculate sparsity
    nnz_tot = 0;
    dim_tot = 0;
    for(auto i : range1(N))
        {
        nnz_tot += nnz(psi(i));
        dim_tot += dim(inds(psi(i)));
        }
    /*printf("Output: \n");    
    PrintData(nnz_tot);
    PrintData(dim_tot);
    printfln("\n");

    PrintData(psi);*/



    //
    // Print the final energy reported by DMRG
    //
    printfln("\nGround State Energy = %.10f",energy);
    printfln("\nUsing inner = %.10f", inner(psi,H,psi) );

    return 0;
    }
