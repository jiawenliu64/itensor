echo "2" >> results_hubbard.txt
export OMP_NUM_THREADS=2
./hubbard_2d >> results_hubbard.txt

echo "6" >> results_hubbard.txt
export OMP_NUM_THREADS=6
./hubbard_2d >> results_hubbard.txt

echo "12" >> results_hubbard.txt
export OMP_NUM_THREADS=12
./hubbard_2d >> results_hubbard.txt

echo "24" >> results_hubbard.txt
export OMP_NUM_THREADS=24
./hubbard_2d >> results_hubbard.txt

echo "48" >> results_hubbard.txt
export OMP_NUM_THREADS=48
./hubbard_2d >> results_hubbard.txt
